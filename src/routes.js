// default

import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// Pages

import Home from '@/pages/Home'
import notFound from '@/pages/404'
import Shop from '@/pages/Shop'
import Product from '@/pages/Product'

//Routering

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/shop',
            name: 'shop',
            component: Shop
        },
        {
            path: '*',
            name: 'notFound',
            component: notFound
        },
        {
            path: '/shop/:id',
            name: 'product',
            component: Product
        }
    ]
})
